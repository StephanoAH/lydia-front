import { useEffect, useMemo, useState } from "react";
import Navbar from "../../components/navbar/navbar";
import axiosInstance from "../../commons/utils/axiosUtils";
import server from "../../commons/const/const";
import Files from "../../commons/interfaces/Files.interface";
import { format } from "date-fns";
import "./csvReport.css";
import KeywordTable from "../../components/tables/csv-table/CsvTable";
import Spinner from "../../components/spinner/spinner";
import { useLocation } from "react-router-dom";
import { parseGeneralReport } from "../../utils/functions";

function CsvReport() {
  const location = useLocation()
  const [fileData, setFileData] = useState<Files[]>([]);
  const [data, setData] = useState([]);
  const [formData, setFormData] = useState({
    selectedCsvId: null,
  });
  const [loading, setLoading] = useState(false)

  const isGeneralReport = useMemo(()=>location.pathname == '/report',[location])
  const fetchData = async () => {
    try {
    setLoading(true)

      const response = await axiosInstance.get(`${server.backend_host}/search-results${isGeneralReport ?'':'/'+formData.selectedCsvId}`);
      setData(isGeneralReport ? parseGeneralReport(response.data) : response.data.keywords);
    } catch (error) {
      console.error('Error fetching data:', error);
    }finally{
    setLoading(false)

    }
  };
  const handleInputChange = (e: any) => {
    const { value } = e.target;
    console.log('value', value);
    setFormData((prev) => ({
      ...prev,
      ['selectedCsvId']: value,
    }));
  };
  
  const handleFormSubmit = (e: React.FormEvent) => {
    e.preventDefault();
    if (formData.selectedCsvId) fetchData()
  };

  useEffect(() => {
    axiosInstance
      .get(`${server.backend_host}/read-csv`)
      .then((response) => {
        setFileData(response.data);
      })
      .catch((error) => {
        console.log("error reading csv: ", error);
      })

      if(isGeneralReport) fetchData()
  }, [location]);

  const columns = useMemo(
    () => {
      const columsGeneric = [
        {
          Header: 'Keyword ID',
          accessor: 'keyword_id',
        },
        {
          Header: 'Keyword',
          accessor: 'keyword',
        },
        {
          Header: 'Total Adwords',
          accessor: 'total_adwords',
        },
        {
          Header: 'Total Links',
          accessor: 'total_links',
        },
        {
          Header: 'Total Results',
          accessor: 'total_results',
        },
        {
          Header: 'Total Seconds',
          accessor: 'total_seconds',
        },
        {
          Header: 'Download File',
          accessor: 'html_code',
          Cell: ({ row }) => (
           <div className="flex-center">
             <button className="csv-button" onClick={() => handleDownload(row.original.keyword_id)}>
              Download
            </button>
           </div>
          ),
        },
      ]
      if(isGeneralReport) columsGeneric.unshift({
        Header: 'File name',
        accessor: 'filename',
      })

      return columsGeneric
    },
    [isGeneralReport]
  );

  const handleDownload = async (keywordId) => {
    try {
      // Replace 'YOUR_DOWNLOAD_ENDPOINT' with the actual endpoint to download the file
      const response = await axiosInstance.get(`${server.backend_host}/search-results/html-code/${keywordId}`, {
        responseType: 'blob',
      });
      const blob = new Blob([response.data], { type: 'application/octet-stream' });
      const link = document.createElement('a');
      link.href = URL.createObjectURL(blob);
      link.download = 'html_code.html';
      link.click();
    } catch (error) {
      console.error('Error downloading file:', error);
    }
  };
  return (
    <>
      <Navbar />
      {loading && <Spinner />}
     
     {!isGeneralReport && <div className="csv-container">
        <form className="csv-form" onSubmit={handleFormSubmit}>
          <select
            name="csv"
            id=""
            value={formData.selectedCsvId || ""}
            onChange={handleInputChange}
          >
            {fileData.map((file) => (
              <option key={file.id} value={file.id}>
                {file.document_name}{" "}
                {format(new Date(file.upload_date), "dd-MM-yyyy")}
              </option>
            ))}
          </select>
          <button type="submit" className="csv-button">Search</button>
        </form>
      </div>}
      
      {data.length != 0 ? (<div className="container-table">
        <KeywordTable columns={columns} data={data} />
      </div>
      ) : (
        <div className="flex-center">
          <h1>No data at the moment</h1>
        </div>
      )}
    </>
  );
}

export default CsvReport;
