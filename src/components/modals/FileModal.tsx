import "./FileModal.css";

const FileModal = ({
  filename,
  uploadDate,
  average_adwords,
  average_links,
  average_seconds,
  average_total,
  keywords,
  onClose,
}) => {
  return (
    <div className="file-modal">
      <div className="modal-content">
        <div>
          <div className="title-container">
            <h2>{filename}</h2>
            <h2 onClick={onClose} className="close-button">X</h2>
          </div>
          <h3>Upload Date: {uploadDate}</h3>
          <br />
          <h4 className="title">Content:</h4>
          <p className="keywords">{keywords}</p>
        </div>
        <div className="average-container">
          <div className="averages-indicators">
              <div className="average-indicator">
                  <p>Average links</p>
                  {average_links}
              </div>
              <div className="average-indicator">
                  <p>Average adwords</p>
                  {average_adwords}
              </div>
              <div className="average-indicator">
                  <p>Average total</p>
                  {average_total}
              </div>
              <div className="average-indicator">
                  <p>Average seconds</p>
                  {average_seconds}
              </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default FileModal;
