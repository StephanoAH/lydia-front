interface CsvContent {
  average_adwords: number;
  average_links: number;
  average_seconds: number;
  average_total: number;
  filename: string;
  id: number;
  keyword_ids: number[];
  keywords: string[];
  upload_date: string;
}

export default CsvContent