import "./Card.css";
import { useState } from "react";
import Spinner from "../spinner/spinner";
import FileModal from "../modals/FileModal";
import server from "../../commons/const/const";
import axiosInstance from "../../commons/utils/axiosUtils";
import CsvContent from "../../commons/interfaces/csv.interface";

const formatNumber = (number: number) => {
  return number.toLocaleString();
};

const FileCard = ({ filename, uploadDate, imageUrl, fileId }) => {
  const [modalOpen, setModalOpen] = useState(false);
  const [fileData, setfileData] = useState<CsvContent>(); 
  const [loading, setloading] = useState(false)

  const handleSeeMore = async () => {
    try {
      setloading(true)
      const response = await axiosInstance.get(
        `${server.backend_host}/read-csv/${fileId}`
      );
      setfileData(response.data);
      setModalOpen(true);
    } catch (error) {
      console.error("Error fetching detailed info: ", error);
    }finally{
      setloading(false)
    }
  };

  const handleCloseModal = () => {
    setModalOpen(false);
  };

  return (
    <>
   {loading && <Spinner />}
      <div className="card">
        <div className="card-container">
          <img src={imageUrl} className="card-img-top" alt="File Preview" />
        </div>
        <div className="card-body">
          <h5 className="card-title">{filename}</h5>
          <p className="card-text">Upload Date: {uploadDate}</p>
          <button className="card-button" onClick={handleSeeMore}>
            See More
          </button>
        </div>
      </div>

      {modalOpen && (
        <FileModal
          filename={filename}
          uploadDate={uploadDate}
          average_adwords={fileData!.average_adwords}
          average_links={fileData!.average_links}
          average_seconds={fileData!.average_seconds}
          average_total={formatNumber(fileData!.average_total)}
          keywords={fileData!.keywords}
          onClose={handleCloseModal}
        />
      )}
    </>
  );
};

export default FileCard;
