const secret_key = import.meta.env.VITE_SECRET_KEY;
const backend_host = import.meta.env.VITE_BACKEND_HOST;
const server = {
  backend_host,
  secret_key,
};

export default server;
