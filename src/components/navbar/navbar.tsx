import './navbar.css';
import { Link } from 'react-router-dom';
import { useAuth } from '../../context/authContext';

const Navbar = () => {
  const { onLogout } = useAuth()
  return (
    <div className="navbar">
      <div className='nav-container'>
        <div className="nav-links">
          <Link to="/dashboard">Dashboard</Link>
          <Link to="/report">Report</Link>
          <Link to="/csv-report">Report by CSV</Link>
          <div className='signOut-container'>
            <button className='SignOut-btn' onClick={onLogout}>
              Sign out
            </button>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Navbar;