import "./FileModal.css";
import IProps from "../../commons/interfaces/modal.interface";

const Modal = ({children,onClose,title,open}:IProps) => {
    return (
      open &&  <div className="file-modal">
            <div className="modal-content">
                <div>
                    <div className="title-container">
                        <h2>{title}</h2>
                        <h2 onClick={onClose} className="close-button">X</h2>
                    </div>
                    {children}
                </div>
               
            </div>
        </div>
    )
}

export default Modal;

