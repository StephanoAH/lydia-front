import { useState, useEffect } from "react";
import SocketIO, { Socket } from "socket.io-client";

const useSocket = (server:string): Socket | null => {
  const [socket, setSocket] = useState<Socket | null>(null);

  useEffect(() => {
    console.log('host',server)
    const newSocket = SocketIO(server,{
      secure: true,
      reconnectionDelay: 500,
      reconnection: true,
      forceNew: true,
  });

  console.log('socket',newSocket)
    setSocket(newSocket);

   
  }, []);

  return socket;
};

export default useSocket;