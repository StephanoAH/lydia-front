import { IReport } from "../interfaces/reports";

export const parseGeneralReport = (report:IReport[]) =>{
    return report.map((item)=>{
        return item.keywords.map(keyword=>({...keyword,filename:item.filename}))
    }).reduce((acc, current) => acc.concat(current), []);
}