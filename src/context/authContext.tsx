import server from "../commons/const/const";
import { useNavigate } from "react-router-dom";
import axiosInstance from "../commons/utils/axiosUtils";
import { createContext, useContext, useEffect, useState } from "react";

export interface IAuthProps {
  authState: {
    token: string | null;
    authenticated: boolean | null;
    user?: any;
  };
  onRegister: (username: string, password: string) => Promise<any>;
  onLogin: (username: string, password: string) => Promise<any>;
  onLogout: () => Promise<any>;
}
const AuthContext = createContext<IAuthProps>({} as IAuthProps);

export const useAuth = () => {
  return useContext(AuthContext);
};

export const AuthProvider = ({ children }: any) => {
  const navigate = useNavigate();

  const [authState, setAuthState] = useState<{
    token: string | null;
    authenticated: boolean | null;
  }>({ token: null, authenticated: null });

  const onRegister = async (
    username: string,
    password: string,
  ) => {
    try {
      const response = await axiosInstance.post(
        `${server.backend_host}/signup`,
        { username, password }
      );
      return response;
    } catch (error) {
      return { error: true, msg: (error as any).response.data.message };
    }
  };

  const onLogin = async (username: string, password: string) => {
    try {
      const result = await axiosInstance.post(`${server.backend_host}/login`, {
        username: username,
        password: password,
      });
      localStorage.setItem("token", result.data.access_token);
      axiosInstance.defaults.headers.common[
        "Authorization"
      ] = `Bearer ${result.data.access_token}`;
      if (result.data.access_token) {
        setAuthState({
          token: result.data.access_token,
          authenticated: true,
        });
      }
      return result;
    } catch (error: any) {
      console.log("error", error);
      return { error: true, msg: error.response.data.message };
    }
  };

  const onLogout = async () => {
    localStorage.removeItem("token");
    axiosInstance.defaults.headers.common["Authorization"] = ``;
    setAuthState({
      authenticated: null,
      token: null,
    });
    navigate("/login");
  };

  const value = {
    authState,
    onLogin,
    onLogout,
    onRegister,
  };

  useEffect(() => {
    const token = localStorage.getItem("token");
    if (token) {
      setAuthState({
        token,
        authenticated: true,
      });
    }
  }, []);

  return <AuthContext.Provider value={value}>{children}</AuthContext.Provider>;
};
