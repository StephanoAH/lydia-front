import { Route, Routes } from 'react-router-dom'
import './App.css'
import AuthForm from './components/auth/auth'
import Dashboard from './pages/dashboard/dashboard'
import { useAuth } from './context/authContext'
import CsvReport from './pages/csvReport/csvReport'
import { setNavigateCallback } from './commons/utils/axiosUtils'

function App() {
  const { authState, onLogout } = useAuth()

  setNavigateCallback(onLogout);
  return (
    <Routes>
      { authState?.authenticated ? 
        <>
          <Route path='/dashboard' element={<Dashboard />} />
          <Route path='/report' element={<CsvReport />} />
          <Route path='/csv-report' element={<CsvReport />} />
          <Route path='/*' element={<Dashboard />} />
        </>
        : 
        <Route path='/*' element={<AuthForm />} /> 
      }
    </Routes>
  )
}

export default App
