import "./auth.css";
import { useState } from "react";
import Spinner from "../spinner/spinner";
import { useNavigate } from "react-router-dom";
import { useAuth } from "../../context/authContext";
import Modal from "../modals/Modal";

const AuthForm = () => {
  const { onLogin, onRegister } = useAuth();
  const [isLogin, setIsLogin] = useState(true);
  const [isLoading, setIsLoading] = useState(false);
  const [formData, setFormData] = useState({
    username: "",
    password: "",
  });
  const [modalOpen, setModalOpen] = useState(false)
  const navigate = useNavigate();

  const handlePanelSwitch = () => {
    setIsLogin((prev) => !prev);
  };

  const handleInputChange = (e: any) => {
    const { name, value } = e.target;
    setFormData((prev) => ({
      ...prev,
      [name]: value,
    }));
  };

  const handleSignIn = async (e: any) => {
    e.preventDefault();
    try {
      setIsLoading(true);
      await onLogin(formData.username, formData.password);
      setIsLoading(false);
      return navigate("/dashboard");
    } catch (error) {
      setIsLoading(false);
      setModalOpen(true)
      console.error("Error submitting form:", error);
    }
  };

  const handleSignUp = async (e: any) => {
    e.preventDefault();
    try {
      setIsLoading(true);
      await onRegister(formData.username, formData.password)
      setFormData({
        username: "",
        password: "",
      })
      setIsLogin(true)
      setIsLoading(false);
    } catch (error) {
      setIsLoading(false);
      setModalOpen(true)
      console.error("Error submitting form:", error);
    }
  };

  return (
    <>
      {isLoading && <Spinner />}
      <div className="bodyContainer">
        <div className={`container ${isLogin ? "" : "right-panel-active"}`}>
          <div className="form-container colanetRE-container">
            <form method="post" onSubmit={handleSignUp}>
              <h1 className="Title">{isLogin ? "Sign in" : "Sign up"}</h1>
              <input
                type="text"
                placeholder="User"
                name="username"
                value={formData.username}
                onChange={handleInputChange}
              />
              <input
                type="password"
                placeholder="Password"
                name="password"
                value={formData.password}
                onChange={handleInputChange}
              />
              <button type="submit" className="auth-btn">
                Sign up!
              </button>
            </form>
          </div>
          <div className="form-container colanetINT-container">
            <form method="post" onSubmit={handleSignIn}>
              <h1 className="Title">{isLogin ? "Sign in" : "Sign up"}</h1>
              <input
                type="text"
                placeholder="User"
                name="username"
                value={formData.username}
                onChange={handleInputChange}
              />
              <input
                type="password"
                placeholder="Password"
                name="password"
                value={formData.password}
                onChange={handleInputChange}
              />
              <button type="submit" className="auth-btn">
                Sign in!
              </button>
            </form>
          </div>
          <div className="overlay-container">
            <div className="overlay">
              <div className="overlay-panel overlay-left">
                <h1 className="Title">Hello, User!</h1>
                <p className="overlay-text">
                  Enter your personal details and start your journey with us
                </p>
                <button className="auth-btn-ghost" onClick={handlePanelSwitch}>
                  Go to sign in
                </button>
              </div>
              <div className="overlay-panel overlay-right">
                <h1 className="Title">Glad to see you back!</h1>
                <p className="overlay-text">Enter your credentials and continue your journey with us</p>
                <button className="auth-btn-ghost" onClick={handlePanelSwitch}>
                  Go to sign up
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
      <Modal open={modalOpen} title="Error" onClose={() => setModalOpen(false)}  >
        <div className="modal-container">
          <h1 className="error-msg">An error has ocurred, please try again.</h1>
        </div>
      </Modal>
    </>
  );
};

export default AuthForm;
