export interface IReport {
    filename:    string;
    id:          number;
    keywords:    Keyword[];
    upload_date: Date;
}

export interface Keyword {
    keyword:          string;
    keyword_id:       number;
    record_id:        number;
    search_result_id: number;
    total_adwords:    number;
    total_links:      number;
    total_results:    number;
    total_seconds:    number;
}