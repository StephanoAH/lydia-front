import FileCard from "../../components/cards/Card";
import Navbar from "../../components/navbar/navbar";
import CsvImage from "../../assets/8242984.png";
import { useEffect, useState } from "react";
import axiosInstance from "../../commons/utils/axiosUtils";
import server from "../../commons/const/const";
import './dashboard.css'
import { format } from 'date-fns';
import Files from "../../commons/interfaces/Files.interface";
import Modal from "../../components/modals/Modal";
import UploadFile from "../../components/uploadFile/UploadFile";
import useSocket from "../../hooks/useSocket";


function Dashboard() {
  const [fileData, setFileData] = useState<Files[]>([]);
  const [modalOpen, setModalOpen] = useState(false)
  const socket = useSocket(server.backend_host)

  const fetchCSV = () =>{
    axiosInstance
    .get(`${server.backend_host}/read-csv`)
    .then((response) => {
      setFileData(response.data);
    })
    .catch((error) => {
      console.log("error reading csv: ", error);
    });

  }
  useEffect(() => {
    fetchCSV()
  }, []);
  useEffect(() => {
    socket?.connect()
    socket?.on('connect', () => {
      console.log('socket conectado :D')

    })

    socket?.on('update_status', (data) => {
      console.log('update',data)
      //alert(JSON.stringify(data))
    })
    socket?.emit('test', "123")

    return () => {
      socket?.off('connect', () => { });

      socket?.off('update_status', () => { });
    }
  }, [socket])


  return (
    <> 
      <Navbar />
      {fileData.length > 0 ?
        <div className="dashboard-container">
        
          {fileData.map((file) => (
            <FileCard
              key={file.id}
              filename={file.document_name}
              uploadDate={format(new Date(file.upload_date), 'dd-MM-yyyy HH:mm')}
              imageUrl={CsvImage}
              fileId={file.id}
            />
          ))}
        </div>
        :
        <div>
          <h1>No csv for this user</h1>
        </div>}
      <Modal open={modalOpen} title="Upload file" onClose={() => setModalOpen(false)}  >
        <UploadFile updateData={fetchCSV} socket={socket} />
      </Modal>
      <button onClick={() => setModalOpen(true)} className="btn-upload">+</button>
    </>
  );
}

export default Dashboard;
