import { useState } from 'react';
import './uploadfile.css'
import ICON_CSV from '../../assets/8242984.png'
import axiosInstance from '../../commons/utils/axiosUtils';
import server from '../../commons/const/const';
import { Socket } from "socket.io-client";

interface IProps {
    socket: Socket | null

    updateData: () => void
}

const UploadFile = ({ socket, updateData }: IProps) => {

    const [selectedFile, setSelectedFile] = useState<any>(null);
    const [loading, setloading] = useState(false)
    const [responseMessage, setResponseMessage] = useState({
        success: false,
        message: '',
        show: false
    })
    const handleFileChange = (event) => {
        setSelectedFile(event.target.files[0]);
    };
    const submitsaveFile = async () => {
        try {

            const formData = new FormData();
            formData.append('csv_file', selectedFile)
            setloading(true)
            const response = await axiosInstance.post(server.backend_host + '/csv', formData)
            setResponseMessage({
                message: "We are processing the csv, please wait",
                success: true,
                show: true
            })
            socket?.emit('search_csv', response.data.message)
            updateData()

        } catch (error: any) {
            setResponseMessage({
                message: error.response.data.message,
                success: false,
                show: true
            })
        } finally {
            setloading(false)
        }
    }




    return (
        !responseMessage.show ? <div className="file-upload">
            {selectedFile == null ? <div className="image-upload-wrap">
                <input onChange={handleFileChange} onClick={handleFileChange} className="file-upload-input" type='file' accept=".csv" />
                <div className="drag-text">
                    <h3>Drag and drop a file or select add csv file</h3>
                </div>
            </div> :
                <div className="file-upload-content">
                    <img className="file-upload-image" src={ICON_CSV} alt="your csv" />
                    <span className='name-file'>{selectedFile?.name}</span>
                    <div className="image-title-wrap">
                        <button type="button" onClick={() => setSelectedFile(null)} className="remove-image">Remove <span className="image-title">Uploaded Csv</span></button>
                        <button type="button" disabled={loading} onClick={submitsaveFile} className="send-image">SAVE <span className="image-title">Uploaded Csv</span></button>
                    </div>
                </div>
            }
        </div> :
            <div className='center-item'>
                <div className={`alert-${responseMessage.success ? 'success' : 'error'}`}>
                    {responseMessage.message}
                </div>
                <button onClick={() => {
                    setSelectedFile(null)
                    setResponseMessage({
                        message: '',
                        success: false,
                        show: false
                    })
                }} className="send-image">Upload another file</button>
            </div>
    )
}

export default UploadFile