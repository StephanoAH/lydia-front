// axiosConfig.js
import axios from 'axios';
import server from '../const/const';
import isTokenExpiringSoon from './tokenUtils';

let navigateCallback;

export const setNavigateCallback = (callback) => {
  navigateCallback = callback;
};

const axiosInstance = axios.create();

axiosInstance.interceptors.request.use(
  async (config) => {
    const token = localStorage.getItem("token")
    if (isTokenExpiringSoon(token)) {
      try {
        const response = await axiosInstance.get(`${server.backend_host}/refresh-token`);
        const newToken = response.data.access_token;
        localStorage.setItem('token', newToken);
        config.headers.Authorization = `Bearer ${newToken}`;
        return config
      } catch (error) {
        console.error('Token refresh failed:', error);
        navigateCallback && navigateCallback('/login');
      }
    }

    if (token !== null) {
      config.headers.Authorization = `Bearer ${token}`;
    }
    return config;
  },
  (error) => {
    return Promise.reject(error);
  }
);

axiosInstance.interceptors.response.use(
  (response) => {
    return response;
  },
  (error) => {
    console.log("axiosInstance error", error.response.status)
    if (error.response && error.response.status === 401) {
      navigateCallback && navigateCallback();
      
    }
    return Promise.reject(error);
  }
);

export default axiosInstance;
