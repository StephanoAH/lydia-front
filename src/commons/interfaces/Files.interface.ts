interface Files {
    id: number,
    document_name: string,
    upload_date: string,
  }

export default Files