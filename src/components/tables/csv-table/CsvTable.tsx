import { useTable } from 'react-table';
import './CsvTable.css'
interface IColumn {
    Header?: string,
    accessor?: string,
    Cell?: ({ row }: { row: any; })=>JSX.Element
}
interface IProps{
  data:any[],
  columns: IColumn[]
}
const KeywordTable = ({data,columns}:IProps) => {

/*   useEffect(() => {
    console.log('changed',csv_id);
    const fetchData = async () => {
      try {
        const response = await axiosInstance.get(`${server.backend_host}/search-results/${csv_id}`);
        setData(response.data.keywords);
      } catch (error) {
        console.error('Error fetching data:', error);
      }
    };
    fetchData();
  }, [csv_id]); */

/*   const columns = useMemo(
    () => [
      {
        Header: 'Keyword ID',
        accessor: 'keyword_id',
      },
      {
        Header: 'Keyword',
        accessor: 'keyword',
      },
      {
        Header: 'Total Adwords',
        accessor: 'total_adwords',
      },
      {
        Header: 'Total Links',
        accessor: 'total_links',
      },
      {
        Header: 'Total Results',
        accessor: 'total_results',
      },
      {
        Header: 'Total Seconds',
        accessor: 'total_seconds',
      },
      {
        Header: 'Download File',
        accessor: 'html_code',
        Cell: ({ row }) => (
          <button onClick={() => handleDownload(row.original.keyword_id)}>
            Download
          </button>
        ),
      },
    ],
    []
  ); */

  const {
    getTableProps,
    getTableBodyProps,
    headerGroups,
    rows,
    prepareRow,
  } = useTable({ columns, data });

/*   const handleDownload = async (keywordId) => {
    try {
      // Replace 'YOUR_DOWNLOAD_ENDPOINT' with the actual endpoint to download the file
      const response = await axiosInstance.get(`${server.backend_host}/search-results/html-code/${keywordId}`, {
        responseType: 'blob',
      });
      const blob = new Blob([response.data], { type: 'application/octet-stream' });
      const link = document.createElement('a');
      link.href = URL.createObjectURL(blob);
      link.download = 'html_code.htm';
      link.click();
    } catch (error) {
      console.error('Error downloading file:', error);
    }
  }; */

  return (
    <table {...getTableProps()} style={{ width: '100%', borderCollapse: 'collapse' }}>
      <thead>
        {headerGroups.map(headerGroup => (
          <tr {...headerGroup.getHeaderGroupProps()}>
            {headerGroup.headers.map(column => (
              <th {...column.getHeaderProps()} style={{ padding: '8px', border: '1px solid #ddd' }}>
                {column.render('Header')}
              </th>
            ))}
          </tr>
        ))}
      </thead>
      <tbody {...getTableBodyProps()}>
        {rows.map(row => {
          prepareRow(row);
          return (
            <tr {...row.getRowProps()} key={row.id}>
              {row.cells.map(cell => (
                <td {...cell.getCellProps()} style={{ padding: '8px', border: '1px solid #ddd' }}>
                  {cell.render('Cell')}
                </td>
              ))}
            </tr>
          );
        })}
      </tbody>
    </table>
  );
};

export default KeywordTable;
