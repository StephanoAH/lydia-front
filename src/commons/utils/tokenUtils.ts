import { jwtDecode } from "jwt-decode";

// Function to check if a JWT token is expired or will expire in the next 5 minutes
const isTokenExpiringSoon = (token) => {
    if (!token) {
      return false;
    }
  
    try {
      const decodedToken = jwtDecode(token);
  
      if (!decodedToken || !decodedToken.exp) {
        return false;
      }
  
      const currentTime = Math.floor(Date.now() / 1000);
      const timeUntilExpiration = decodedToken.exp - currentTime;
  
      return timeUntilExpiration > 0 && timeUntilExpiration <= 5 * 60;
    } catch (error) {
      console.error('Error decoding token:', error);
      return false;
    }
  };
  
  export default isTokenExpiringSoon;