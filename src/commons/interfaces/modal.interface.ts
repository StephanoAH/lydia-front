interface IProps{
  title:string
  onClose?:()=>void
  children:any
  open:boolean
}

export default IProps